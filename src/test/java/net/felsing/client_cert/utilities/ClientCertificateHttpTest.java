package net.felsing.client_cert.utilities;

import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleHttpResponse;
import org.apache.hc.client5.http.async.methods.SimpleRequestBuilder;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.async.HttpAsyncClients;
import org.apache.hc.client5.http.impl.nio.PoolingAsyncClientConnectionManager;
import org.apache.hc.client5.http.impl.nio.PoolingAsyncClientConnectionManagerBuilder;
import org.apache.hc.client5.http.protocol.HttpClientContext;
import org.apache.hc.client5.http.routing.HttpRoutePlanner;
import org.apache.hc.client5.http.ssl.ClientTlsStrategyBuilder;
import org.apache.hc.client5.http.ssl.NoopHostnameVerifier;
import org.apache.hc.core5.http.HttpResponse;
import org.apache.hc.core5.http.Method;
import org.apache.hc.core5.http.nio.ssl.TlsStrategy;
import org.apache.hc.core5.http.protocol.HttpContext;
import org.apache.hc.core5.reactor.IOReactorConfig;
import org.apache.hc.core5.ssl.SSLContexts;
import org.apache.hc.core5.ssl.TrustStrategy;
import org.junit.Test;

import javax.net.ssl.SSLContext;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import static org.junit.Assert.assertEquals;

// see https://www.baeldung.com/httpasyncclient-tutorial

public class ClientCertificateHttpTest {

    @Test
    public void testAsyncHttpClient() throws ExecutionException, InterruptedException, IOException {
        final String HOST_WITH_COOKIE = "https://ip6.li";
        final SimpleHttpRequest request = SimpleRequestBuilder.get(HOST_WITH_COOKIE)
                .build();
        final CloseableHttpAsyncClient client = HttpAsyncClients.custom()
                .build();
        client.start();


        final Future<SimpleHttpResponse> future = client.execute(request, null);
        final HttpResponse response = future.get();

        assertEquals(response.getCode(), 200);
        client.close();
    }

    @Test
    public void whenUseMultipleHttpAsyncClient_thenCorrect() throws Exception {

        //final HttpRoutePlanner proxy = new HttpHost("127.0.0.1", 3306);
        final HttpRoutePlanner proxy = null;

        final IOReactorConfig ioReactorConfig = IOReactorConfig
                .custom()
                .build();

        CloseableHttpAsyncClient client;
        if (proxy==null) {
            client = HttpAsyncClients.custom()
                    .setIOReactorConfig(ioReactorConfig)
                    .build();
        } else {
            client = HttpAsyncClients.custom()
                    .setRoutePlanner(proxy)
                    .setIOReactorConfig(ioReactorConfig)
                    .build();
        }
        client.start();
        final String[] toGet = { "http://www.google.com/", "http://www.apache.org/", "http://www.bing.com/" };

        final GetThread[] threads = new GetThread[toGet.length];
        for (int i = 0; i < threads.length; i++) {
            final SimpleHttpRequest request = new SimpleHttpRequest(Method.GET.name(), toGet[i]);
            threads[i] = new GetThread(client, request);
        }

        for (final GetThread thread : threads) {
            thread.start();
        }

        for (final GetThread thread : threads) {
            thread.join();
        }
    }

    static class GetThread extends Thread {

        private final CloseableHttpAsyncClient client;
        private final HttpContext context;
        private final SimpleHttpRequest request;

        GetThread(final CloseableHttpAsyncClient client, final SimpleHttpRequest request) {
            this.client = client;
            context = HttpClientContext.create();
            this.request = request;
        }

        @Override
        public void run() {
            try {
                final Future<SimpleHttpResponse> future = client.execute(request, context, null);
                final HttpResponse response = future.get();
                assertEquals(response.getCode(), 200);
            } catch (final Exception ex) {
                System.out.println(ex.getLocalizedMessage());
            }
        }

    }


    @Test
    public void whenUseSSLWithHttpAsyncClient_thenCorrect() throws Exception {
        final String HOST_WITH_SSL = "https://ip6.li";
        final TrustStrategy acceptingTrustStrategy = (certificate, authType) -> true;

        KeyStore keyStore = KeyStore.getInstance("PKCS12");
        keyStore.load(new FileInputStream("cert.p12"), "Geheim1234".toCharArray());

        final SSLContext sslContext = SSLContexts.custom()
                .loadTrustMaterial(null, acceptingTrustStrategy)
                .loadKeyMaterial(keyStore, "Geheim1234".toCharArray())
                .build();

        final TlsStrategy tlsStrategy = ClientTlsStrategyBuilder.create()
                .setHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setSslContext(sslContext)
                .build();

        final PoolingAsyncClientConnectionManager cm = PoolingAsyncClientConnectionManagerBuilder.create()
                .setTlsStrategy(tlsStrategy)
                .build();

        final CloseableHttpAsyncClient client = HttpAsyncClients.custom()
                .setConnectionManager(cm)
                .build();

        client.start();

        final SimpleHttpRequest request = new SimpleHttpRequest("GET",HOST_WITH_SSL);
        final Future<SimpleHttpResponse> future = client.execute(request, null);

        final HttpResponse response = future.get();
        assertEquals(response.getCode(), 200);
        client.close();
    }



}
